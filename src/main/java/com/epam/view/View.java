package com.epam.view;

import com.epam.Application;
import com.epam.controller.Controller;
import com.epam.controller.ControllerIml;
import com.epam.model.First;
import com.epam.model.TestForAnnotations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", ("1 - Own annotation"));
        menu.put("2", ("2 - Invoke method "));
        menu.put("3", ("3 - Info about class"));
        menu.put("Q", ("Q - Exit"));
    }

    public View() {

        controller = new ControllerIml();
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("Q", this::pressButton4);

    }

    private void pressButton1() {
        Class ref = TestForAnnotations.class;
        Field[] fields = ref.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(First.class)) {
                First first = field.getAnnotation(First.class);
                String nam = first.name();
                int ag = first.age();
                logger1.info(field.getName());
                logger1.info(nam);
                logger1.info(ag);
            }
        }
    }

    private void pressButton2() {
        TestForAnnotations inv = new TestForAnnotations();
        Class ref = TestForAnnotations.class;
        try {
            Method method = ref.getDeclaredMethod("getName");
            Method method1 = ref.getDeclaredMethod("getCount");
            Method method2 = ref.getDeclaredMethod("getValue");
            method.invoke(inv);
            method2.invoke(inv);
            method1.invoke(inv);

        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void pressButton3() {
        Class ref = TestForAnnotations.class;
        logger1.info(ref.getSimpleName());
        logger1.info(ref.getName());
        Field[] fields = ref.getDeclaredFields();
        for (Field f : fields) {
            logger1.info(f.getName());
        }
        Method[] method = ref.getMethods();
        for (Method m : method) {
            logger1.info(m.getName());
        }

    }

    private void pressButton4() {
        logger1.info("Bye-Bye");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nAnnotations:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


