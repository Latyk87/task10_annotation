package com.epam.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation for test
 * Created by Borys Latyk on 02/12/2019.
 *
 * @version 2.1
 * @since 02.12.2019
 */
@Target(value = ElementType.FIELD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface First {
    String name() default "Human";

    int age() default 18;
}
