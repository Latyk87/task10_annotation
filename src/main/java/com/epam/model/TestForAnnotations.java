package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class use the annotations.
 *
 * @version 2.1
 * Created by Borys Latyk on 02/12/2019.
 * @since 02.12.2019
 */
public class TestForAnnotations {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    @First(name = "Petro")
    private int count;
    @First(name = "Ivan")
    private String name;
    private int value;

    public String getName() {
        logger1.info("Somebody invoked me");
        return name;
    }

    public int getValue() {
        logger1.info("Me also");
        return value;
    }

    public int getCount() {
        logger1.info("OMG!!!its API Reflection");
        return count;
    }


    @Override
    public String toString() {
        return "TestForAnnotations{" +
                "count=" + value +
                '}';
    }
}
